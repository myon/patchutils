#!/usr/bin/make -f

DEB_HOST_GNU_TYPE  ?= $(shell dpkg-architecture -qDEB_HOST_GNU_TYPE)
DEB_BUILD_GNU_TYPE ?= $(shell dpkg-architecture -qDEB_BUILD_GNU_TYPE)

ifeq ($(DEB_BUILD_GNU_TYPE), $(DEB_HOST_GNU_TYPE))
CONFFLAGS = --build $(DEB_HOST_GNU_TYPE)
else
CONFFLAGS = --build $(DEB_BUILD_GNU_TYPE) --host $(DEB_HOST_GNU_TYPE)
endif

CFLAGS = -g

ifeq (,$(findstring noopt,$(DEB_BUILD_OPTIONS)))
CFLAGS += -O2
endif

D=$(CURDIR)/debian/patchutils/usr

configure: configure-stamp
configure-stamp:
	dh_testdir
	dh_autoreconf
	CFLAGS="$(CFLAGS)" ./configure $(CONFFLAGS) --prefix=/usr --mandir=\$${prefix}/share/man
	touch configure-stamp

build build-arch: build-stamp
build-stamp: configure-stamp
	dh_testdir
	# the Makefile runs some stuff twice with -jN
	$(MAKE) -j1
	# Manual files that use the .so links to include other pages should
	# point to a path relative to the top-level manual hierarchy
	sed -i -e 's/ rediff.1/ man1\/rediff.1/' doc/editdiff.1
	$(MAKE) check -j1
	touch build-stamp

build-indep:

clean:
	dh_testdir
	dh_testroot
	rm -f build-stamp configure-stamp
	[ ! -f Makefile ] || $(MAKE) distclean
	dh_autoreconf_clean
	dh_clean doc/*.1 doc/*.6

install: build-stamp
	dh_testdir
	dh_testroot
	dh_prep
	dh_installdirs
	$(MAKE) install prefix=$D
	mkdir $D/games
	mv $D/bin/espdiff $D/games

binary-arch: install
	dh_testdir
	dh_testroot
	dh_installchangelogs ChangeLog
	dh_installdocs AUTHORS BUGS NEWS TODO
	dh_link
	dh_strip
	dh_compress
	dh_fixperms
	dh_installdeb
	dh_perl
	dh_shlibdeps
	dh_gencontrol
	dh_md5sums
	dh_builddeb

binary-indep:

binary: binary-indep binary-arch

.PHONY: configure build clean install binary-arch binary-indep binary
